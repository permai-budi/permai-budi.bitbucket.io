var myOptions = {
    autoplay: true,
    autoplaySpeed: 3000,
    dots: false,
    prevArrow: '<i class="fa fa-chevron-left slick-prev" aria-hidden="true"></i>',
    nextArrow: '<i class="fa fa-chevron-right slick-next" aria-hidden="true"></i>',
    draggable: false,
    arrows: false,
    slidesToShow: 1,
}



$('.header-hamburger').click( function() {
  $('.header-nav').addClass('open-h');
  $('body').addClass('stop-scrolling');
});
$('.header-close').click( function() {
  $('.header-nav').removeClass('open-h');
  $('body').removeClass('stop-scrolling');
});

$('.mySlider').slick(myOptions);

// VIDEO POPUP
//
// Open popup
$('.open-pop').click(function() {
    $('.popup').css('display', 'flex');
    
    setTimeout( function() {
      $('.popup').addClass('open');
      $('body').addClass('stop-scrolling');
    },0);
    
  });
  
// Close Popup
  $('.popup__mask, .popup__close').click(function() {
    
    $('.popup').removeClass('open');
    $('body').removeClass('stop-scrolling');
    $('.youtube-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
    console.log("textx");
    //is this working

    setTimeout( function() {
      $('.popup').css('display', 'none');
      
    }, 401);
  });

// GSAP Portion
//
var transition = 0.75;
var y_value = 200;
//HOME
//hero
gsap.from(".hero-text", { scrollTrigger: ".hero-text", y: y_value, opacity: 0, duration: transition});

//partners
gsap.from(".partners .partner-img", { scrollTrigger: ".partners .partner-img", y: y_value, opacity: 0, duration: transition});

//numbers
gsap.from(".box-number", { scrollTrigger: ".box-number", y: y_value, opacity: 0, duration: transition});

//info-col
gsap.from(".high-school .info-col", { scrollTrigger: ".high-school .info-col", y: y_value, opacity: 0, duration: transition});
gsap.from(".cambodia-project .info-col", { scrollTrigger: ".cambodia-project .info-col", y: y_value, opacity: 0, duration: transition});

//cta
gsap.from(".cta-box", { scrollTrigger: ".cta-box", y: y_value, opacity: 0, duration: transition});


//ABOUI
//
//intro
gsap.from(".intro .row", { scrollTrigger: ".intro .row", y: y_value, opacity: 0, duration: transition});

//beginning
gsap.from(".beginning .row", { scrollTrigger: ".beginning .row", y: y_value, opacity: 0, duration: transition});

//chairman
gsap.from(".founder .info-col", { scrollTrigger: ".founder .info-col", y: y_value, opacity: 0, duration: transition});

//people
gsap.from("#people-row-1", { scrollTrigger: "#people-row-1", y: y_value, opacity: 0, duration: transition});
gsap.from("#people-row-2", { scrollTrigger: "#people-row-2", y: y_value, opacity: 0, duration: transition});
gsap.from("#people-row-3", { scrollTrigger: "#people-row-3", y: y_value, opacity: 0, duration: transition});


//WORK
//
//video
// gsap.from(".video-box", { scrollTrigger: ".video-box", y: y_value, opacity: 0, duration: transition});

//big text
gsap.from(".big-text h2", { scrollTrigger: ".big-text h2", y: y_value, opacity: 0, duration: transition});

//fickry-hs
gsap.from(".fickry-hs .info-col", { scrollTrigger: ".fickry-hs .info-col", y: y_value, opacity: 0, duration: transition});

gsap.from(".the-cambodia-project .info-col", { scrollTrigger: ".the-cambodia-project .info-col", y: y_value, opacity: 0, duration: transition});

//donation
gsap.from(".donate-img", { scrollTrigger: ".donate-img", y: y_value, opacity: 0, duration: transition});


//CONTACT
//
//contact info
gsap.from(".hero .contact-info", { scrollTrigger: ".hero .contact-info", y: y_value, opacity: 0, duration: transition});

//form
gsap.from("#my-form", { scrollTrigger: "#my-form", y: y_value, opacity: 0, duration: transition});